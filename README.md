# EDZIF validator

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

Library to validate DNS zones in EDZIF format.

## Example usage

To validate a zone (ES6 syntax, Node.js):

    const { Zone } = require('edzif-validator');

    Zone(zoneData).then((result) => {
        // Check result.valid to see if we got a valid response.
        // Check result.errors to see any errors returned.
    });

## Local Development

To run the test suite whenever the code changes, run:

    npm run dev

Below is a list of commands you will probably find useful.

### `npm start` or `yarn start`

Runs the project in development/watch mode. Your project will be rebuilt upon changes. TSDX has a special logger for you convenience. Error messages are pretty printed and formatted for compatibility VS Code's Problems tab.

Your library will be rebuilt if you make edits.

### `npm run build` or `yarn build`

Bundles the package to the `dist` folder.
The package is optimized and bundled with Rollup into multiple formats (CommonJS, UMD, and ES Module).

### `npm test` or `yarn test`

Runs the test watcher (Jest) in an interactive mode.
By default, runs tests related to files changed since the last commit.

## Mad props

This project was bootstrapped with [TSDX](https://github.com/jaredpalmer/tsdx).
