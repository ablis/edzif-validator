import { edzifZoneSchema } from '../src/index'

describe('stub test for index.ts', () => {
  it('should export our public API', () => {
    expect(edzifZoneSchema).toBeDefined()
  })
})
