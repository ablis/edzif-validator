import { domainNameSchema } from '../../src/validators/domain_name'

describe('domainNameSchema', () => {
  const schema = domainNameSchema
  it('should pass example.com', () => {
    const input = 'example.com'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('should pass træls.dk (punycode)', () => {
    const input = 'xn--trls-woa.dk'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('træls.dk (raw)', () => {
    const input = 'træls.dk'
    return expect(schema.validate(input)).rejects.toThrow(
      'must match the following'
    )
  })

  it('rhabarberbarbarabarbarbarenbartbarbierbierbar.com', () => {
    const input = 'rhabarberbarbarabarbarbarenbartbarbierbierbar.com'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('topgear.co.uk', () => {
    const input = 'topgear.co.uk'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('xn--fdbk5d8ap9b8a8d.xn--deba0ad', () => {
    const input = 'xn--fdbk5d8ap9b8a8d.xn--deba0ad'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('xn--e1afmkfd.xn--80akhbyknj4f', () => {
    const input = 'xn--e1afmkfd.xn--80akhbyknj4f'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('-bjarne.dk', () => {
    const input = '-bjarne.dk'
    return expect(schema.validate(input)).rejects.toThrow(
      'must match the following'
    )
  })

  it('bjarne-.dk', () => {
    const input = 'bjarne-.dk'
    return expect(schema.validate(input)).rejects.toThrow(
      'must match the following'
    )
  })
})
