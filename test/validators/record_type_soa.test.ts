import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeSOA,
  recordTypeSOASchema
} from '../../src/validators/record_type_soa'

describe('SOA record tests', () => {
  const schema = recordTypeSOASchema

  it('valid SOA record', () => {
    const record: RecordTypeSOA = {
      name: 'example.com',
      record_type: EdzifRecordType.SOA,
      ttl: 43200,
      primary_server: 'ns1.example.com',
      responsible_person: 'zonemaster.example.com',
      serial: 2016062942,
      refresh: 10,
      retry: 10,
      expire: 10,
      minimum_ttl: 10
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('SOA record with negative TTL', () => {
    const record = {
      name: 'example.com',
      record_type: EdzifRecordType.SOA,
      ttl: -4,
      primary_server: 'ns1.example.com',
      responsible_person: 'zonemaster.example.com',
      serial: 2016062942,
      refresh: 10,
      retry: 10,
      expire: 10,
      minimum_ttl: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'ttl must be a positive number'
    )
  })

  it('SOA record with missing name', () => {
    const record = {
      record_type: EdzifRecordType.SOA,
      ttl: -4,
      primary_server: 'ns1.example.com',
      responsible_person: 'zonemaster.example.com',
      serial: 2016062942,
      refresh: 10,
      retry: 10,
      expire: 10,
      minimum_ttl: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'name is a required field'
    )
  })
})
