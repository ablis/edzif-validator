import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeCNAME,
  recordTypeCNAMESchema
} from '../../src/validators/record_type_cname'

describe('CNAME record type tests', () => {
  const schema = recordTypeCNAMESchema
  it('valid CNAME record', () => {
    const record: RecordTypeCNAME = {
      id: 42,
      prefix: 'www',
      zone_name: 'example.com',
      record_type: EdzifRecordType.CNAME,
      ttl: 43200,
      name: 'example.com'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('valid CNAME record with name xn--e1afmkfd.xn--80akhbyknj4f', () => {
    const record: RecordTypeCNAME = {
      id: 42,
      prefix: 'xn--e1afmkfd',
      zone_name: 'example.com',
      record_type: EdzifRecordType.CNAME,
      ttl: 43200,
      // Cyrillic script domain name.
      name: 'xn--e1afmkfd.xn--80akhbyknj4f'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('CNAME record with invalid type', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: 'ALIAS',
      ttl: 43200,
      name: 'example.net'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type must be one of the following values: CNAME'
    )
  })

  it('CNAME with invalid name -bjarne.dk', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.CNAME,
      ttl: 43200,
      name: '-bjarne.dk'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'name must match the following'
    )
  })

  it('CNAME with invalid name bjarne-.dk', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.CNAME,
      ttl: 43200,
      name: 'bjarne-.dk'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'name must match the following'
    )
  })
})
