import { edzifZoneSchema } from '../../src/validators/zone'

describe('zone validation tests (example.com)', () => {
  it('valid zone', () => {
    const zone = require('../fixtures/zone_full_valid')

    return expect(edzifZoneSchema.validate(zone)).resolves.toBe(zone)
  })
})
