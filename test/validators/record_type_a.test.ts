import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeA,
  recordTypeASchema
} from '../../src/validators/record_type_a'

describe('record type A', () => {
  const schema = recordTypeASchema
  it('should validate a valid A record', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.A,
      ttl: 43200,
      address: '192.168.1.42'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('should fail A record with invalid IP address', () => {
    const record: RecordTypeA = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.A,
      ttl: 43200,
      address: '192.168.42'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'address must be a valid IPv4 address'
    )
  })

  it('should fail A record with IP address with CIDR', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.A,
      ttl: 43200,
      address: '192.168.42.0/16'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'address must be a valid IPv4 address'
    )
  })

  it('A record with invalid type', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: 'B',
      ttl: 43200,
      address: '192.168.42.1'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type must be one of the following values: A'
    )
  })
})
