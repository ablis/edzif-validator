import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeNS,
  recordTypeNSSchema
} from '../../src/validators/record_type_ns'

describe('NS record type tests', () => {
  const schema = recordTypeNSSchema

  it('valid NS record', () => {
    const record: RecordTypeNS = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.NS,
      ttl: 43200,
      name: 'ns1.example.com'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('valid NS record with name xn--e1afmkfd.xn--80akhbyknj4f', () => {
    const record: RecordTypeNS = {
      id: 42,
      prefix: 'xn--e1afmkfd',
      zone_name: 'example.com',
      record_type: EdzifRecordType.NS,
      ttl: 43200,
      // Cyrillic script domain name.
      name: 'xn--e1afmkfd.xn--80akhbyknj4f'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('NS record with invalid type', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: 'ALIAS',
      ttl: 43200,
      name: 'example.net'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type must be one of the following values: NS'
    )
  })

  it('NS with invalid name -bjarne.dk', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.NS,
      ttl: 43200,
      name: '-bjarne.dk'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'name must match the following'
    )
  })

  it('NS with invalid name bjarne-.dk', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.NS,
      ttl: 43200,
      name: 'bjarne-.dk'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'name must match the following'
    )
  })
})
