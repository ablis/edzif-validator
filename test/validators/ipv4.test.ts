import { ipv4Schema } from '../../src/validators/ipv4'

const nonStringInput = [false, true, NaN, 0, 200, {}, [], [1]]

const invalidIpAddresses = [
  undefined,
  '0.0.0',
  '1.1.1.1.1',
  '307.007.007.007',
  '1984.34.2.4',
  '-1.-1.-1.-1',
  '139.3.3.307',
  '255.255.255.256'
]

const validIpAddresses = [
  '0.0.0.0',
  '1.1.1.1',
  '007.007.007.007',
  '139.30.03.3',
  '255.255.255.255'
]

describe('IPv4 address schema', () => {
  it('should fail non-string input', async () => {
    for (const address of nonStringInput) {
      await expect(ipv4Schema.validate(address)).rejects.toThrow(
        'must be a `string`'
      )
    }
  })

  it('should fail invalid IP addresses', async () => {
    for (const address of invalidIpAddresses) {
      await expect(ipv4Schema.validate(address)).rejects.toThrow(
        'must be a valid IPv4 address'
      )
    }
  })

  it('should pass valid IP addresses', async () => {
    for (const address of validIpAddresses) {
      await expect(ipv4Schema.validate(address)).resolves.toBe(address)
    }
  })
})
