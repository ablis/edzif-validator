import { ipv6Schema } from '../../src/validators/ipv6'

const nonStringInput = [false, true, NaN, 0, 200, {}, [], [1]]

const invalidIpAddresses = [
  undefined,
  '-1.-1.-1.-1',
  '0.0.0.0',
  '007.007.007.007',
  '1.1.1.1',
  '139.3.3.307',
  '198.34.2.4',
  '1:1:1:1:1:1:1:1:1',
  '1::1::1',
  '255.255.255.256'
]

const validIpAddresses = [
  '0:0:0:0:0:0:0:0',
  '0:0:0:0:0:0:0:1',
  '1080:0:0:0:8:800:200C:417A',
  '1111:2222:3333:4444:5555:6666:7777:8888',
  '2001:0DB8:0:0:8:800:200C:417A',
  '2001:0DB8::8:800:200C:417A',
  '::',
  '::1',
  'FEDC:BA98:7654:3210:FEDC:BA98:7654:3210',
  'FF01:0:0:0:0:0:0:101',
  'FF01::101'
]

describe('IPv6 address schema', () => {
  it('should fail non-string input', async () => {
    for (const address of nonStringInput) {
      await expect(ipv6Schema.validate(address)).rejects.toThrow(
        'must be a `string`'
      )
    }
  })

  it('should fail invalid IP addresses', async () => {
    for (const address of invalidIpAddresses) {
      await expect(ipv6Schema.validate(address)).rejects.toThrow(
        'must be a valid IPv6 address'
      )
    }
  })

  it('should pass valid IP addresses', async () => {
    for (const address of validIpAddresses) {
      await expect(ipv6Schema.validate(address)).resolves.toBe(address)
    }
  })
})
