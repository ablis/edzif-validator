import { zoneNameSchema } from '../../src/validators/zone_name'

describe('zoneNameSchema', () => {
  const schema = zoneNameSchema

  it('example.com', () => {
    const input = 'example.com'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('træls.dk (punycode)', () => {
    const input = 'xn--trls-woa.dk'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('træls.dk (raw)', () => {
    const input = 'træls.dk'
    return expect(schema.validate(input)).rejects.toThrow(
      'must match the following'
    )
  })

  it('rhabarberbarbarabarbarbarenbartbarbierbierbar.com', () => {
    const input = 'rhabarberbarbarabarbarbarenbartbarbierbierbar.com'
    return expect(schema.validate(input)).resolves.toBe(input)
  })

  it('topgear.co.uk', () => {
    const input = 'topgear.co.uk'
    return expect(schema.validate(input)).resolves.toBe(input)
  })
})
