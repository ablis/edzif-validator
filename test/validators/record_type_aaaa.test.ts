import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeAAAA,
  recordTypeAAAASchema
} from '../../src/validators/record_type_aaaa'

describe('recordTypeAAAASchema', () => {
  const schema = recordTypeAAAASchema

  it('should pass a valid AAAA record', () => {
    const record: RecordTypeAAAA = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.AAAA,
      ttl: 43200,
      address: 'fdda:5cc1:23:4::1f'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('should fail a AAAA record with invalid IP address', () => {
    const record: RecordTypeAAAA = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.AAAA,
      ttl: 43200,
      address: '192.168.42.42'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'address must be a valid IPv6 address'
    )
  })

  it('should fail a AAAA record with IP address with CIDR', () => {
    const record: RecordTypeAAAA = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.AAAA,
      ttl: 43200,
      address: 'beef:dead::0/16'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'address must be a valid IPv6 address'
    )
  })

  it('should fail a AAAA record with invalid type', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: 'ABB',
      ttl: 43200,
      address: 'beef:dead::1'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type must be one of the following values: AAAA'
    )
  })
})
