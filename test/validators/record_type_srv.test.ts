import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeSRV,
  recordTypeSRVSchema
} from '../../src/validators/record_type_srv'

describe('SRV record tests', () => {
  const schema = recordTypeSRVSchema

  it('valid SRV record', () => {
    const record: RecordTypeSRV = {
      prefix: '_submission._tcp',
      zone_name: 'example.com',
      record_type: EdzifRecordType.SRV,
      ttl: 43200,
      name: 'smtp.example.com',
      port: 587,
      priority: 10,
      weight: 10
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('SRV record with negative port', () => {
    const record: RecordTypeSRV = {
      prefix: '_submission._tcp',
      zone_name: 'example.com',
      record_type: EdzifRecordType.SRV,
      ttl: 43200,
      name: 'smtp.example.com',
      port: -587,
      priority: 10,
      weight: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'port must be a positive number'
    )
  })

  it('SRV record with port over 65535', () => {
    const record: RecordTypeSRV = {
      prefix: '_submission._tcp',
      zone_name: 'example.com',
      record_type: EdzifRecordType.SRV,
      ttl: 43200,
      name: 'smtp.example.com',
      port: 72000,
      priority: 10,
      weight: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'port must be less than or equal to 65535'
    )
  })

  it('SRV record with missing name', () => {
    const record = {
      prefix: '_submission._tcp',
      zone_name: 'example.com',
      record_type: EdzifRecordType.SRV,
      ttl: 43200,
      port: 587,
      priority: 10,
      weight: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'name is a required field'
    )
  })

  it('SRV record with missing port', () => {
    const record = {
      prefix: '_submission._tcp',
      zone_name: 'example.com',
      record_type: EdzifRecordType.SRV,
      ttl: 43200,
      name: 'smtp.example.com',
      priority: 10,
      weight: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'port is a required field'
    )
  })

  it('SRV record with invalid type', () => {
    const record = {
      prefix: '_submission._tcp',
      zone_name: 'example.com',
      record_type: 'SAV',
      ttl: 43200,
      name: 'smtp.example.com',
      port: 10,
      priority: 10,
      weight: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type must be one of the following values: SRV'
    )
  })
})
