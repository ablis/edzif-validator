import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeTXT,
  recordTypeTXTSchema
} from '../../src/validators/record_type_txt'

describe('record type TXT tests', () => {
  const schema = recordTypeTXTSchema

  it('valid TXT record', () => {
    const record: RecordTypeTXT = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.TXT,
      ttl: 43200,
      text_content: 'v=spf1 mx -all'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('valid TXT record with DMARC value', () => {
    const record: RecordTypeTXT = {
      id: 42,
      prefix: '_dmarc',
      zone_name: 'example.com',
      record_type: EdzifRecordType.TXT,
      ttl: 43200,
      text_content: 'v=DMARC1; p=reject'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('valid TXT record with DKIM value', () => {
    const record: RecordTypeTXT = {
      id: 42,
      prefix: 'mail._domainkey',
      zone_name: 'example.com',
      record_type: EdzifRecordType.TXT,
      ttl: 43200,
      text_content:
        'v=DKIM1; k=rsa; s=email; p=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaa+aaaaaaaaa'
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('TXT record with invalid type', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: 'TEXT',
      ttl: 43200,
      text_content: 'v=spf1 mx -all'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type must be one of the following values: TXT'
    )
  })

  it('TXT record with invalid content “ø”', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.TXT,
      ttl: 43200,
      text_content: 'v=spf1 møx -all'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'text_content must match the following'
    )
  })

  it('TXT record with invalid content “💩”', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      record_type: EdzifRecordType.TXT,
      ttl: 43200,
      text_content: '💩'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'text_content must match the following'
    )
  })
})
