import { edzifZoneSchema } from '../../src/validators/zone'

describe('edzifZoneSchema', () => {
  it('should pass a basic zone', () => {
    const zone = require('../fixtures/zone_basic')

    return expect(edzifZoneSchema.validate(zone)).resolves.toBe(zone)
  })

  it('should pass zone with new GTLD zone name', () => {
    const zone = require('../fixtures/zone_basic_new_gtld')

    return expect(edzifZoneSchema.validate(zone)).resolves.toBe(zone)
  })

  it('should fail zone without name property', () => {
    const zone = require('../fixtures/zone_no_name')

    return expect(edzifZoneSchema.validate(zone)).rejects.toThrow(
      'name is a required field'
    )
  })

  it('should fail zone with extraneous property', () => {
    const zone = require('../fixtures/zone_extra_prop')

    return expect(edzifZoneSchema.validate(zone)).rejects.toThrow(
      'field cannot have keys not specified'
    )
  })

  it('should pass zone with all allowed properties', () => {
    const zone = require('../fixtures/zone_allowed_props')

    return expect(edzifZoneSchema.validate(zone)).resolves.toBe(zone)
  })
})
