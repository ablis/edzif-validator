import { EdzifRecordType } from '../../src/types/EdzifRecord'
import { RecordBase, recordBaseSchema } from '../../src/validators/record_base'

describe('record base tests', () => {
  const schema = recordBaseSchema

  it('valid base record', () => {
    const record: RecordBase = {
      id: 42,
      prefix: '',
      zone_name: 'example.com',
      record_type: EdzifRecordType.A,
      ttl: 43200
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('valid base record with prefix', () => {
    const record: RecordBase = {
      id: 42,
      prefix: 'me',
      zone_name: 'example.com',
      record_type: EdzifRecordType.AAAA,
      ttl: 43200
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('valid base record with no zone name or prefix', () => {
    const record: RecordBase = {
      id: 42,
      record_type: EdzifRecordType.CNAME,
      ttl: 43200
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('base record with missing record type', () => {
    const record = {
      id: 42,
      zone_name: 'example.com',
      ttl: 424
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type is a required field'
    )
  })

  it('base record with invalid TTL', () => {
    const record = {
      id: 42,
      prefix: 'me',
      zone_name: 'example.com',
      record_type: EdzifRecordType.SRV,
      ttl: -37
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'ttl must be a positive number'
    )
  })
})
