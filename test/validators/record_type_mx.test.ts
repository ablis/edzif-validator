import { EdzifRecordType } from '../../src/types/EdzifRecord'
import {
  RecordTypeMX,
  recordTypeMXSchema
} from '../../src/validators/record_type_mx'

describe('MX record tests', () => {
  const schema = recordTypeMXSchema

  it('valid MX record', () => {
    const record: RecordTypeMX = {
      zone_name: 'example.com',
      record_type: EdzifRecordType.MX,
      ttl: 43200,
      name: 'smtp.example.com',
      priority: 10
    }

    return expect(schema.validate(record)).resolves.toBe(record)
  })

  it('MX record with invalid priority', () => {
    const record: RecordTypeMX = {
      zone_name: 'example.com',
      record_type: EdzifRecordType.MX,
      ttl: 43200,
      name: 'smtp.example.com',
      priority: -1
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'priority must be a positive number'
    )
  })

  it('MX record with missing name', () => {
    const record = {
      zone_name: 'example.com',
      record_type: EdzifRecordType.MX,
      ttl: 43200,
      priority: 100
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'name is a required field'
    )
  })

  it('MX record with missing priority', () => {
    const record = {
      zone_name: 'example.com',
      record_type: EdzifRecordType.MX,
      ttl: 43200,
      name: 'smtp.example.com'
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'priority is a required field'
    )
  })

  it('MX record with invalid type', () => {
    const record = {
      zone_name: 'example.com',
      record_type: 'MAX',
      ttl: 43200,
      name: 'smtp.example.com',
      priority: 10
    }

    return expect(schema.validate(record)).rejects.toThrow(
      'record_type must be one of the following values: MX'
    )
  })
})
