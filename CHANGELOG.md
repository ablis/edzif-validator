# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.0.0-alpha.1](https://github.com/mikl/edzif-validator/compare/v3.0.0-alpha.0...v3.0.0-alpha.1) (2019-06-16)


### Features

* add enum for record type ([a9f5be7](https://github.com/mikl/edzif-validator/commit/a9f5be7))



## [3.0.0-alpha.0](https://github.com/mikl/edzif-validator/compare/v2.1.0...v3.0.0-alpha.0) (2019-05-25)

### Features

- convert everything to TypeScript ([c1b7280](https://github.com/mikl/edzif-validator/commit/c1b7280))
- replace joi with yup ([773fec0](https://github.com/mikl/edzif-validator/commit/773fec0))

### BREAKING CHANGES

- completely new promise-based API.

Yup is much lighter than joi, which removes the need for a separate
browser build.
