import { RecordTypeA } from '../validators/record_type_a'
import { RecordTypeAAAA } from '../validators/record_type_aaaa'
import { RecordTypeCNAME } from '../validators/record_type_cname'
import { RecordTypeMX } from '../validators/record_type_mx'
import { RecordTypeNS } from '../validators/record_type_ns'
import { RecordTypeSOA } from '../validators/record_type_soa'
import { RecordTypeSRV } from '../validators/record_type_srv'
import { RecordTypeTXT } from '../validators/record_type_txt'

export type EdzifRecord =
  | RecordTypeA
  | RecordTypeAAAA
  | RecordTypeCNAME
  | RecordTypeMX
  | RecordTypeNS
  | RecordTypeSOA
  | RecordTypeSRV
  | RecordTypeTXT

export enum EdzifRecordType {
  A = 'A',
  AAAA = 'AAAA',
  CNAME = 'CNAME',
  MX = 'MX',
  NS = 'NS',
  SOA = 'SOA',
  SRV = 'SRV',
  TXT = 'TXT'
}
