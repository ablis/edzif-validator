import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { domainNameSchema } from './domain_name'
import { RecordBase, recordBaseSchema } from './record_base'

export interface RecordTypeSRV extends RecordBase {
  record_type: EdzifRecordType.SRV
  name: string
  port: number
  priority: number
  weight: number
}

export const recordTypeSRVSchema = recordBaseSchema
  .shape({
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.SRV])
      .required(),
    name: domainNameSchema.required(),
    port: yup
      .number()
      .integer()
      .positive()
      .max(65535)
      .required(),
    priority: yup
      .number()
      .integer()
      .min(0)
      .max(500)
      .required(),
    weight: yup
      .number()
      .integer()
      .min(0)
      .max(500)
      .required()
  })
  .noUnknown()
  .strict(true)
