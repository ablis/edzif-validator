import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { domainNameSchema } from './domain_name'
import { RecordBase, recordBaseSchema } from './record_base'

// @see https://blog.bobcares.com/understanding-soa-records/
// @see http://rscott.org/dns/soa.html

export interface RecordTypeSOA extends RecordBase {
  record_type: EdzifRecordType.SOA
  name: string
  primary_server: string
  responsible_person: string
  serial: number
  refresh: number
  retry: number
  expire: number
  minimum_ttl: number
}

export const recordTypeSOASchema = recordBaseSchema
  .shape({
    name: domainNameSchema.required(),
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.SOA])
      .required(),
    ttl: yup
      .number()
      .integer()
      .positive(),
    primary_server: domainNameSchema.required(),
    responsible_person: domainNameSchema.required(),
    serial: yup
      .number()
      .integer()
      .positive()
      .required(),
    refresh: yup
      .number()
      .integer()
      .positive()
      .required(),
    retry: yup
      .number()
      .integer()
      .positive()
      .required(),
    expire: yup
      .number()
      .integer()
      .positive()
      .required(),
    minimum_ttl: yup
      .number()
      .integer()
      .positive()
      .required()
  })
  .noUnknown()
  .strict(true)
