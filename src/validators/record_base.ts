import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { ZoneName, zoneNameSchema } from './zone_name'

export interface RecordBase {
  id?: number | string
  prefix?: string
  zone_name?: ZoneName
  record_type: EdzifRecordType
  ttl?: number
}

export const recordBaseSchema = yup
  .object({
    // Implementation specific, any kind of ID is allowed.
    id: yup.mixed(),
    prefix: yup.string(),
    zone_name: zoneNameSchema,
    record_type: yup
      .string()
      .oneOf(Object.keys(EdzifRecordType))
      .required(),
    ttl: yup
      .number()
      .integer()
      .positive()
  })
  .noUnknown()
  .strict(true)
