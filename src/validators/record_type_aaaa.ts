import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { ipv6Schema } from './ipv6'
import { RecordBase, recordBaseSchema } from './record_base'

export interface RecordTypeAAAA extends RecordBase {
  record_type: EdzifRecordType.AAAA
  address: string
}

export const recordTypeAAAASchema = recordBaseSchema
  .shape({
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.AAAA])
      .required(),
    address: ipv6Schema.required()
  })
  .noUnknown()
  .strict(true)
