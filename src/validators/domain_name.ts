import * as yup from 'yup'

export type DomainName = string

export const domainNameSchema = yup
  .string()
  .matches(
    /^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/
  )
  .max(253)
  .strict(true)
