import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { domainNameSchema } from './domain_name'
import { RecordBase, recordBaseSchema } from './record_base'

export interface RecordTypeMX extends RecordBase {
  record_type: EdzifRecordType.MX
  name: string
  priority: number
}

export const recordTypeMXSchema = recordBaseSchema
  .shape({
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.MX])
      .required(),
    name: domainNameSchema.required(), //.allow('@'),
    priority: yup
      .number()
      .integer()
      .positive()
      .max(500)
      .required()
  })
  .noUnknown()
  .strict(true)
