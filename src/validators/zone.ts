import * as yup from 'yup'
import { EdzifRecord } from '../types/EdzifRecord'
import { edzifRecordSchema } from './edzif_record'
import { zoneNameSchema } from './zone_name'

export interface EdzifZone {
  id?: number | string
  name: string
  records: EdzifRecord[]
  vendor?: unknown
}

export const edzifZoneSchema = yup
  .object()
  .shape({
    // Implementation specific, any kind of ID is allowed.
    id: yup.mixed(),
    name: zoneNameSchema.required(),
    records: yup.array().of(edzifRecordSchema),
    vendor: yup.object()
  })
  .noUnknown()
  .strict(true)
