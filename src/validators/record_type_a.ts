import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { ipv4Schema } from './ipv4'
import { RecordBase, recordBaseSchema } from './record_base'

export interface RecordTypeA extends RecordBase {
  record_type: EdzifRecordType.A
  address: string
}

export const recordTypeASchema = recordBaseSchema
  .shape({
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.A])
      .required(),
    address: ipv4Schema.required()
  })
  .noUnknown()
  .strict(true)
