import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { domainNameSchema } from './domain_name'
import { RecordBase, recordBaseSchema } from './record_base'

export interface RecordTypeNS extends RecordBase {
  record_type: EdzifRecordType.NS
  name: string
}

export const recordTypeNSSchema = recordBaseSchema
  .shape({
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.NS])
      .required(),
    name: domainNameSchema.required()
  })
  .noUnknown()
  .strict(true)
