import * as yup from 'yup'

const doubleColonMatcher = /::/g
const fourHexDigitsMatcher = /^[0-9a-fA-F]{1,4}$/

const validIpv6Part = (value: string): boolean => {
  return !!value.match(fourHexDigitsMatcher)
}

const validIpv6 = (value: unknown): boolean => {
  if (typeof value !== 'string') return false

  const doubleColonCount = value.match(doubleColonMatcher)

  if (doubleColonCount && doubleColonCount.length > 1) {
    return false
  }

  const parts = value.split(':')

  let partCount = 0

  for (const part of parts) {
    partCount += 1
    if (part !== '' && !validIpv6Part(part)) {
      return false
    }
  }

  if (partCount > 8) {
    return false
  }

  return true
}

export const ipv6Schema = yup
  .string()
  .test('ipv6', '${path} must be a valid IPv6 address', (value: unknown) => {
    return validIpv6(value)
  })
  .strict(true)
