import * as yup from 'yup'
import { EdzifRecord } from '../types/EdzifRecord'
import { recordTypeASchema } from './record_type_a'
import { recordTypeAAAASchema } from './record_type_aaaa'
import { recordTypeCNAMESchema } from './record_type_cname'
import { recordTypeMXSchema } from './record_type_mx'
import { recordTypeNSSchema } from './record_type_ns'
import { recordTypeSOASchema } from './record_type_soa'
import { recordTypeSRVSchema } from './record_type_srv'
import { recordTypeTXTSchema } from './record_type_txt'

interface RecordTypeSchemas {
  [propName: string]: yup.ObjectSchema<any>
}

const schemas: RecordTypeSchemas = {
  A: recordTypeASchema,
  AAAA: recordTypeAAAASchema,
  CNAME: recordTypeCNAMESchema,
  MX: recordTypeMXSchema,
  NS: recordTypeNSSchema,
  SOA: recordTypeSOASchema,
  SRV: recordTypeSRVSchema,
  TXT: recordTypeTXTSchema
}

export const validateEdzifRecord = (record: EdzifRecord) => {
  const schema = schemas[record.record_type]

  if (schema) {
    return schema.validate(record, {
      abortEarly: false
    })
  } else {
    throw new Error(
      'EDZIFVALDERR-001: No validator found for record type ' +
        record.record_type
    )
  }
}

export const edzifRecordSchema = yup
  .object()
  .test('edzif-record', '${path} is not a valid edzif-record', async value => {
    const result = await validateEdzifRecord(value)
    return result.error ? result.error : true
  })
