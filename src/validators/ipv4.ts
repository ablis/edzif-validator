import * as yup from 'yup'

const threeDigitNumberMatcher = /^[0-9]{1,3}$/

const validIpv4Octet = (value: string): boolean => {
  if (!value.match(threeDigitNumberMatcher)) return false

  const number = Number(value)

  if (number < 0 || number > 255) return false

  return true
}

const validIpv4 = (value: unknown): boolean => {
  if (typeof value !== 'string') return false

  const parts = value.split('.')

  // IPV4 addresses must have four octets.
  if (parts.length !== 4) return false

  for (const part of parts) {
    if (!validIpv4Octet(part)) return false
  }

  return true
}

export const ipv4Schema = yup
  .string()
  .test('ipv4', '${path} must be a valid IPv4 address', (value: unknown) => {
    return validIpv4(value)
  })
  .strict(true)
