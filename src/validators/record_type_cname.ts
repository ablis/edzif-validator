import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { domainNameSchema } from './domain_name'
import { RecordBase, recordBaseSchema } from './record_base'

export interface RecordTypeCNAME extends RecordBase {
  record_type: EdzifRecordType.CNAME
  name: string
}

export const recordTypeCNAMESchema = recordBaseSchema
  .shape({
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.CNAME])
      .required(),
    name: domainNameSchema.required() //.allow('@')
  })
  .noUnknown()
  .strict(true)
