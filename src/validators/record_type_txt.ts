import * as yup from 'yup'
import { EdzifRecordType } from '../types/EdzifRecord'
import { RecordBase, recordBaseSchema } from './record_base'

export interface RecordTypeTXT extends RecordBase {
  record_type: EdzifRecordType.TXT
  text_content: string
}

export const recordTypeTXTSchema = recordBaseSchema
  .shape({
    record_type: yup
      .string()
      .oneOf([EdzifRecordType.TXT])
      .required(),
    // Allowed in TXT records are ASCII letters, plus a selected set of
    // symbols, which translates to this gobbledygook you see in the regex
    // below. The \[\\\]\ sequence just whitelists the characters [\], but
    // since they all need escaping in regex, it becomes a bit strange.
    text_content: yup
      .string()
      .required()
      .matches(/^[A-Za-z0-9 !"#\$%&'()*+,-.\/:;<=>?@\[\\\]\^_`{\|}~-]+$/)
  })
  .noUnknown()
  .strict(true)
