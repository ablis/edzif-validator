import * as yup from 'yup'

export type ZoneName = string

export const zoneNameSchema = yup
  .string()
  .matches(/^[a-zA-Z0-9][a-zA-Z0-9\.-]*\.[a-zA-Z]{2,}$/)
  .strict(true)
